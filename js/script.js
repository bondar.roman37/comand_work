// Input Testimonials_forma
const validEmail = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu;
const inputForma1 = document.querySelector('input');

function isEmailValid_1(value) {
    return validEmail.test(value);
}

function onInput_1() {
    if (isEmailValid_1(inputForma1.value)) {
        inputForma1.style.borderColor = 'green';
        console.log('input valid')
    } else {
        inputForma1.style.borderColor = 'red';
        console.log('input not valid')
    }
}

inputForma1.addEventListener('input', onInput_1);

// Input Footer_forma
const inputForma2 = document.querySelector('.input_2');

function isEmailValid_2(value) {
    return validEmail.test(value);
}

function onInput_2() {
    if (isEmailValid_2(inputForma2.value)) {
        inputForma2.style.borderColor = 'green';
        console.log('input valid')
    } else {
        inputForma2.style.borderColor = 'red';
        console.log('input not valid')
    }
}

inputForma2.addEventListener('input', onInput_2);
function openModal () {
    Toastify({
        text: "This is a toast",
        className: "info",
        style: {
            background: "linear-gradient(to right, #00b09b, #96c93d)",
        }
    }).showToast();
}
document.querySelectorAll('.modal_click').forEach(el => el.addEventListener('click', openModal))



